import pymongo
from .serializers import DataSerializer


class Data:
    def __init__(self, timestamp: float, temp: float, hum: float, press: float, voc: int):
        self.timestamp = timestamp
        self.temperature = temp
        self.humidity = hum
        self.pressure = press
        self.voc = voc


class MongoDb:
    def __init__(self):
        self.__client = pymongo.MongoClient('mongodb://localhost:27017/')
        self.__db = self.__client['inz']
        self.__collection = self.__db['measurements']

    def insert_data(self, data: Data):
        serializer = DataSerializer(data)
        self.__collection.insert_one(serializer.data)

    def find(self, last: int, from_timestamp: int, measurement_type: str) -> dict:
        data = self.__collection.find({
            'timestamp': {
                '$gt': from_timestamp
            }
        }, {
            '_id': 0
        }).sort('timestamp', pymongo.DESCENDING)

        if last > 0:
            data.limit(last)

        measurements = dict()
        for record in data:
            if type(record) is not dict or 'timestamp' not in record:
                continue

            timestamp = record['timestamp']
            for key in record:
                if key == 'timestamp':
                    continue
                elif key not in measurements:
                    measurements[key] = list()

                if record[key] is not None:
                    measurements[key].append({
                        'timestamp': int(timestamp*1000),
                        'value': record[key]
                    })

        unusable_measurements = list()
        for key in measurements:
            if len(measurements[key]) < 50:
                unusable_measurements.append(key)

        for key in unusable_measurements:
            measurements.pop(key)

        if measurement_type is not None:
            for key in measurements:
                if key != measurement_type:
                    measurements[key].clear()
                    
        return measurements

    def __del__(self):
        self.__client.close()
