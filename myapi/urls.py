# from django.conf.urls import include
from django.urls import path, include
from .views import *

urlpatterns = [
    path('addMeasurements', AddMeasurements.as_view()),
    path('getMeasurements', GetMeasurements.as_view()),
]