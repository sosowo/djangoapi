from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Data, MongoDb
from time import time


class AddMeasurements(APIView):
    def post(self, request, *args, **kwargs):
        data = Data(
            time(),
            request.data.get('temperature'),
            request.data.get('humidity'),
            request.data.get('pressure'),
            request.data.get('voc')
        )

        db = MongoDb()
        db.insert_data(data)

        return Response({'Status': 'OK'}, status.HTTP_200_OK)


def get_variable_from_request(request, variable_name: str):
    variable = request.GET.get(variable_name)
    if variable:
        if not variable.isnumeric():
            raise ValueError('"' + variable_name + '" must be positive integer')
        variable = int(variable)
    else:
        variable = 0

    return variable


class GetMeasurements(APIView):
    def get(self, request, *args, **kwargs):
        last = get_variable_from_request(request, 'last')
        from_time = get_variable_from_request(request, 'from')
        measurement_type = request.GET.get('measurement_type', None)

        db = MongoDb()
        return Response(db.find(last, from_time, measurement_type), status.HTTP_200_OK)

