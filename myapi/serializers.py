from rest_framework import serializers


class DataSerializer(serializers.Serializer):
    timestamp = serializers.FloatField()
    temperature = serializers.FloatField()
    humidity = serializers.FloatField()
    pressure = serializers.FloatField()
    voc = serializers.IntegerField()

